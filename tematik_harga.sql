-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 05, 2023 at 03:52 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tematik_harga`
--

-- --------------------------------------------------------

--
-- Table structure for table `harga`
--

CREATE TABLE `harga` (
  `id` int(11) NOT NULL,
  `kode_provinsi` varchar(2) DEFAULT NULL,
  `kode_kabupaten` varchar(2) DEFAULT NULL,
  `komoditas` varchar(100) DEFAULT NULL,
  `harga` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `harga`
--

INSERT INTO `harga` (`id`, `kode_provinsi`, `kode_kabupaten`, `komoditas`, `harga`) VALUES
(1, '62', '04', 'Bandeng', 35000),
(2, '62', '04', 'Gula Pasir', 15000),
(3, '62', '04', 'Beras', 230000),
(4, '62', '04', 'Daging Sapi', 125000),
(5, '62', '13', 'Minyak Goreng Curah', 14000),
(6, '62', '13', 'Gula Pasir', 16000),
(7, '62', '13', 'Beras', 230000),
(8, '62', '13', 'Daging Sapi', 125000),
(9, '62', '05', 'Bandeng', 36000),
(10, '62', '05', 'Minyak Goreng Curah', 14500),
(11, '62', '05', 'Gula Pasir', 16000),
(12, '62', '05', 'Beras', 230000),
(13, '62', '05', 'Daging Sapi', 125000),
(14, '62', '10', 'Minyak Goreng Curah', 14500),
(15, '62', '10', 'Gula Pasir', 16000),
(16, '62', '10', 'Beras', 230000),
(17, '62', '10', 'Daging Sapi', 125000),
(18, '62', '03', 'Bandeng', 36000),
(19, '62', '03', 'Minyak Goreng Curah', 14500),
(20, '62', '03', 'Gula Pasir', 16000),
(21, '62', '03', 'Beras', 230000),
(22, '62', '06', 'Minyak Goreng Curah', 14500),
(23, '62', '06', 'Gula Pasir', 16000),
(24, '62', '06', 'Beras', 230000),
(25, '62', '06', 'Daging Sapi', 125000),
(26, '62', '01', 'Gula Pasir', 16000),
(27, '62', '01', 'Beras', 230000),
(28, '62', '01', 'Minyak Goreng Curah', 14500),
(29, '62', '01', 'Gula Pasir', 16000),
(30, '62', '02', 'Daging Sapi', 125000),
(31, '62', '02', 'Gula Pasir', 16000),
(32, '62', '02', 'Beras', 230000),
(33, '62', '02', 'Minyak Goreng Curah', 14500),
(34, '62', '09', 'Gula Pasir', 16000),
(35, '62', '09', 'Daging Sapi', 125000),
(36, '62', '09', 'Gula Pasir', 16000),
(37, '62', '09', 'Beras', 230000),
(38, '62', '12', 'Minyak Goreng Curah', 14500),
(39, '62', '12', 'Gula Pasir', 16000),
(40, '62', '12', 'Daging Sapi', 125000),
(41, '62', '12', 'Gula Pasir', 16000),
(42, '62', '11', 'Beras', 230000),
(43, '62', '11', 'Minyak Goreng Curah', 14500),
(44, '62', '11', 'Gula Pasir', 16000),
(45, '62', '11', 'Daging Sapi', 125000),
(46, '62', '08', 'Daging Sapi', 125000),
(47, '62', '08', 'Gula Pasir', 16000),
(48, '62', '08', 'Beras', 230000),
(49, '62', '08', 'Minyak Goreng Curah', 14500),
(50, '62', '07', 'Beras', 230000),
(51, '62', '07', 'Minyak Goreng Curah', 14500),
(52, '62', '07', 'Gula Pasir', 16000),
(53, '62', '07', 'Daging Sapi', 125000),
(54, '62', '71', 'Minyak Goreng Curah', 14500),
(55, '62', '71', 'Gula Pasir', 16000),
(56, '62', '71', 'Daging Sapi', 125000),
(57, '62', '71', 'Gula Pasir', 16000);

-- --------------------------------------------------------

--
-- Table structure for table `wilayah`
--

CREATE TABLE `wilayah` (
  `id` int(11) NOT NULL,
  `kode_provinsi` varchar(2) DEFAULT NULL,
  `kode_kabupaten` varchar(2) DEFAULT NULL,
  `nama_kabupaten` varchar(100) DEFAULT NULL,
  `ibukota` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `wilayah`
--

INSERT INTO `wilayah` (`id`, `kode_provinsi`, `kode_kabupaten`, `nama_kabupaten`, `ibukota`) VALUES
(1, '62', '04', 'Kabupaten Barito Selatan', 'Buntok'),
(2, '62', '13', 'Kabupaten Barito Timur', 'Tamiang Layang'),
(3, '62', '05', 'Kabupaten Barito Utara', 'Muara Teweh'),
(4, '62', '10', 'Kabupaten Gunung Mas', 'Kuala Kurun'),
(5, '62', '03', 'Kabupaten Kapuas', 'Kuala Kapuas'),
(6, '62', '06', 'Kabupaten Katingan', 'Kasongan'),
(7, '62', '01', 'Kabupaten Kotawaringin Barat', 'Pangkalan Bun'),
(8, '62', '02', 'Kabupaten Kotawaringin Timur', 'Sampit'),
(9, '62', '09', 'Kabupaten Lamandau', 'Nanga Bulik'),
(10, '62', '12', 'Kabupaten Murung Raya', 'Puruk Cahu'),
(11, '62', '11', 'Kabupaten Pulang Pisau', 'Pulang Pisau'),
(12, '62', '08', 'Kabupaten Sukamara', 'Sukamara'),
(13, '62', '07', 'Kabupaten Seruyan', 'Kuala Pembuang'),
(14, '62', '71', 'Kota Palangka Raya', '-');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `harga`
--
ALTER TABLE `harga`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wilayah`
--
ALTER TABLE `wilayah`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `harga`
--
ALTER TABLE `harga`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `wilayah`
--
ALTER TABLE `wilayah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
