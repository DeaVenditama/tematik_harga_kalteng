<!doctype html>
<html lang="en">

<?php
//membaca file db.php untuk melakukan koneksi database;
include 'db.php';
//nama file polygon kalimantan tengah, file polygon tidak bisa berformat shp, harus geojson/json
$fileName = 'kalimantan_tengah.geojson';
//baca file polygon
$file = file_get_contents($fileName);
//ubah file polygon dalam bentuk json agar bisa dibaca PHP
$file = json_decode($file);
//nama komoditas yang ada di database, ganti baris ini untuk melakukan penggantian komoditas sesuai dengan yang ada pada database, tabel harga
$jenis_komoditas = 'Gula Pasir';
//baris 16-24 digunakan untuk mencari harga komoditas pada baris 14, data hasil query disimpan pada variabel $data_bandeng
$query_bandeng = "SELECT * FROM harga WHERE komoditas='" . $jenis_komoditas . "'";
$result_bandeng = $conn->query($query_bandeng);
$data_bandeng = [];

if ($result_bandeng->num_rows > 0) {
    while ($row = $result_bandeng->fetch_assoc()) {
        array_push($data_bandeng, $row);
    }
}
//variabel pada baris 26 digunakan untuk membaca atribut pada file yang disebut pada baris 8
$features = $file->features;
//looping untuk assign harga ke geojson, ini dibutuhkan untuk mewarnai polygon
foreach ($features as $index => $feature) {
    //jika data tidak ada maka data di set 0 agar tidak error
    $features[$index]->properties->harga = 0;
    //looping pada komoditas untuk mencari kode wilayah yang match
    foreach ($data_bandeng as $index_komoditas => $komoditas) {
        //ambil kode wilayah dari hasil query
        $kode_wilayah_db = $komoditas['kode_provinsi'] . '.' . $komoditas['kode_kabupaten'];
        //ambil kode wilayah dari polygon geojson
        $kode_wilayah_polygon = $feature->properties->fid;
        //matching kode wilayah polygon dan database, jika match tambahkan data yang ada dari database ke polygon, menjadi atribut harga
        if ($kode_wilayah_db == $kode_wilayah_polygon) {
            $features[$index]->properties->harga = $komoditas['harga'];
        }
    }
}

//baris 45-52 digunakan untuk mencari harga maximum komoditas baris 14, harga maximum digunakan untuk melihat batas atas harga
$query_nilai_max = "SELECT MAX(harga) AS nilai_max FROM harga WHERE komoditas='" . $jenis_komoditas . "'";
$result_max = $conn->query($query_nilai_max);
$nilai_max = 0;
if ($result_max->num_rows > 0) {
    while ($row = $result_max->fetch_assoc()) {
        $nilai_max = $row['nilai_max'];
    }
}

//$conn->close();
?>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="./bootstrap-5.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="./leaflet/leaflet.css">
    <title>Tematik Harga</title>
    <style>
        #map {
            height: 700px;
        }

        .info {
            padding: 6px 8px;
            font: 14px/16px Arial, Helvetica, sans-serif;
            background: white;
            background: rgba(255, 255, 255, 0.8);
            box-shadow: 0 0 15px rgba(0, 0, 0, 0.2);
            border-radius: 5px;
        }

        .info h4 {
            margin: 0 0 5px;
            color: #777;
        }

        .legend {
            line-height: 18px;
            color: #555;
        }

        .legend i {
            width: 18px;
            height: 18px;
            float: left;
            margin-right: 8px;
            opacity: 0.7;
        }
    </style>
</head>

<body>
    <nav class="navbar navbar-expand-md navbar-dark bg-primary">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">Peta Tematik Harga <?= $jenis_komoditas ?> Kalimantan Tengah</a>
        </div>
    </nav>

    <div class="row">
        <div class="col-md-12">
            <div id="map"></div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="./bootstrap-5.3.1/js/bootstrap.min.js" crossorigin="anonymous"></script>
    <script src="./leaflet/leaflet.js"></script>
    <script>
        $(document).ready(function() {
            //latitude longitude Kalimantan Tengah untuk set default posisi peta
            const latitude = '-1.4227555605689497';
            const longitude = '113.59805957224113';
            //baca file php polygon di javascript agar bisa diolah leaflet
            const data = <?= json_encode($features) ?>;
            //baca data php nilai max di javascript agar bisa diolah
            const nilaiMax = <?= $nilai_max ?>;
            //tampilkan peta dasar pada website
            var map = L.map('map').setView([latitude, longitude], 7);
            L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
                maxZoom: 19,
                attribution: '© OpenStreetMap'
            }).addTo(map);
            //tambahkan layer polygon kalteng pada peta (variabel map)
            var geojson = L.geoJson(data, {
                //memanggil fungsi style pada baris 136 untuk mendefinisikan style polygon
                style: style,
                //memanggil fungsi onEachFeature pada baris 162 untuk mendefinisikan action pada setiap kabupaten yang ada pada polygon
                onEachFeature: onEachFeature
            }).addTo(map);

            //function untuk mendefinisikan style
            function style(feature) {
                return {
                    weight: 1,
                    opacity: 1,
                    color: 'black',
                    dashArray: '3',
                    fillOpacity: 0.7,
                    fillColor: getColor(parseInt(feature.properties.harga))
                };
            }

            //function untuk mengatur warna pada polygon
            function getColor(d) {
                if (d == 0) {
                    return '#f0f0f0';
                }
                return d > parseFloat((nilaiMax - 100)).toFixed(2) ? '#800026' :
                    d > parseFloat((nilaiMax - 200)).toFixed(2) ? '#e67e22' :
                    d > parseFloat((nilaiMax - 300)).toFixed(2) ? '#f1c40f' :
                    '#2ecc71';
            }

            //function yang mendefinisikan aksi pada setiap kabupaten polygon
            function onEachFeature(feature, layer) {
                //tambahkan tooltip/label pada setiap kabupaten
                layer.bindTooltip(feature.properties.kab_kota, {
                    permanent: true
                }).openTooltip();
                //tambahkan aksi highlight pada mouseover
                //tambahkan aksi click ketika user click kabupaten, aksi detail ada pada function onClick baris 179
                layer.on({
                    mouseover: highlightFeature,
                    mouseout: resetHighlight,
                    click: onClick
                });
            }
            //fungsi yang mendefinisikan aksi ketika polygon kabupaten di klik
            function onClick(e) {
                //ambil data kode wilayah, nama_wilayah, kode_provinsi dan kode_kabupaten pada polygon yang di klik
                var properties = e.target.feature.properties;
                var kode_wilayah = properties.kode_kk;
                var nama_wilayah = properties.kab_kota;
                var split_kode_wilayah = kode_wilayah.split(".");
                var kode_provinsi = split_kode_wilayah[0];
                var kode_kabupaten = split_kode_wilayah[1];
                //lakukan ajax, koneksi ke backend untuk mengambil data detail dari kabupaten yang di klik, cek file service.php untuk melihat apa yang dilakukan pada sisi PHP
                $.ajax({
                    url: 'http://localhost:81/tematik_harga/service.php?kode_provinsi=' + kode_provinsi + '&kode_kabupaten=' + kode_kabupaten,
                    type: 'GET',
                    dataType: 'json',
                    success: function(res) {
                        //buat tabel komoditas harga 
                        var table_data = '<table class="table table-bordered">';
                        for (var i = 0; i < res.length; i++) {
                            table_data += '<tr>';
                            table_data += '<td>' + res[i].komoditas + '</td>';
                            table_data += '<td>Rp ' + res[i].harga + '</td>';
                            table_data += '</tr>';
                        }
                        table_data += '</table>';
                        //buat popup berisi tabel pada variabel tabel;
                        var popup = L.popup(e.latlng, {
                            content: '<h4>Kabupaten ' + nama_wilayah + '</h4><br>' + table_data
                        }).openOn(map);
                    }
                });
            }

            //fungsi untuk highlight kabupaten
            function highlightFeature(e) {
                var layer = e.target;

                layer.setStyle({
                    weight: 1,
                    color: '#ff0000',
                    dashArray: '',
                    fillOpacity: 0.7
                });

                if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
                    layer.bringToFront();
                }

                info.update(layer.feature.properties);
            }

            //fungsi untuk menghilangkan highlight
            function resetHighlight(e) {
                geojson.resetStyle(e.target);
                info.update();
            }

            //baris 231-246 digunakan untuk menampilkan info pada pojok kanan atas
            var info = L.control();
            info.onAdd = function(map) {
                this._div = L.DomUtil.create('div', 'info');
                this.update();
                return this._div;
            };
            info.update = function(props) {
                this._div.innerHTML = (props ?
                    '<b>' + props.kab_kota + '</b><br />Harga <?= $jenis_komoditas ?> : ' + (props.harga != 0 ? props.harga : 'Data Tidak Tersedia') + '' :
                    'Hover di atas wilayah');
            };
            info.addTo(map);

            //baris 244-264 digunakan untuk menampilkan legenda pada pojok kanan bawah
            var legend = L.control({
                position: 'bottomright'
            });

            legend.onAdd = function(map) {

                var div = L.DomUtil.create('div', 'info legend'),
                    //grades = [1, parseFloat((nilaiMax / 4) * 1), parseFloat((nilaiMax / 4) * 2), parseFloat((nilaiMax / 4) * 3).toFixed(2)],
                    grades = [1, parseFloat(nilaiMax - 300), parseFloat(nilaiMax - 200), parseFloat(nilaiMax - 100)],
                    labels = [];

                // loop through our density intervals and generate a label with a colored square for each interval
                for (var i = 0; i < grades.length; i++) {
                    div.innerHTML +=
                        '<i style="background:' + getColor(grades[i] + 1) + '"></i> ' +
                        grades[i] + (grades[i + 1] ? '&ndash;' + grades[i + 1] + '<br>' : '+');
                }

                return div;
            };

            legend.addTo(map);
        });
    </script>
</body>

</html>