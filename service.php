<?php
//baca file db.php untuk melakukan koneksi database
include 'db.php';

//jika ada GET request variabel dengan nama kode_provinsi dan kode_kabupaten jalankan aksi dibawahnya
if (isset($_GET['kode_provinsi']) && isset($_GET['kode_kabupaten'])) {
    //ambil kode provinsi
    $kode_provinsi = $_GET['kode_provinsi'];
    //ambil kode kabupaten
    $kode_kabupaten = $_GET['kode_kabupaten'];
    //query ambil seluruh komoditas dan harga pada satu kabupaten
    $query = "SELECT * FROM harga WHERE kode_provinsi='" . $kode_provinsi . "' AND kode_kabupaten='" . $kode_kabupaten . "'";
    $result = $conn->query($query);
    if ($result->num_rows > 0) {
        $all_result = [];
        while ($row = $result->fetch_object()) {
            array_push($all_result, $row);
        }
        //berikan balikan data seluruh komoditas pada kabupaten tertentu
        echo json_encode($all_result);
    } else {
        echo 'error';
    }
} else {
    echo 'error';
}
$conn->close();
